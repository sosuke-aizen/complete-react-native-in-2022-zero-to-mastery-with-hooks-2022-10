

If you are looking to improve your LinkedIn profile and have others endorse
your skills, we have a private ZTM[ **LinkedIn group
here**](https://www.linkedin.com/groups/12121940/) **.** LinkedIn allows you
to have recruiters message you with lots of job opportunities. You can **join
the group by clicking on "LinkedIn Group"** and then go ahead and endorse some
of the member's skills (other people will do the same for you as they join).  
  
If you have any questions, reach out in our private Discord chat community in
the **#job-hunting** channel!  
  
 **UPDATE!!!** **Zero to Mastery is officially a recognized school!** What
does this mean for you? It means that you can add it as an educational
institution on LinkedIn as part of your profile to wow those employers _(as
your education history)_. ****[**Check it out
here**](https://www.linkedin.com/school/64685953/). To add it to your profile:

  

 _Step 1: Go to personal LinkedIn profile_

 _Step 2: Scroll down to the Education section_

 _Step 3: Click the_ ** _+_**

 _Step 4: Type in_ ** _Zero To Mastery Academy_**

