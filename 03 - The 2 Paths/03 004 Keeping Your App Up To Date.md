

Is everything up to date!? This is one of the most common questions we get in
the course. The answer is.... YES!  
  
We have a special section after we build all of our apps in the course titled
**_Keeping Your App Up To Date_** which will show you how to update everything
to the latest versions and showing you how to keep your apps up to date for
all future React Native updates.  
  
Therefore if you are ever curious, you can always jump to that section to
see... but we recommend you save that until the very end :)  

