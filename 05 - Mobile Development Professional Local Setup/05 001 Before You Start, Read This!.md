

Before we get going with installation, it's **super** **important** you
understand the structure below!

Videos pre-faced with:

\- [MAC]: These are for Mac users

\- [WINDOWS]: These are for Windows users

\- [MAC] Optional: These videos are optional for Mac users to do and won't
block you from able to build meals to go

\- [WINDOWS] Optional: These videos are optional for Windows users to do and
won't block you from able to build meals to go

\- Optional: These videos are optional to do and won't block you from able to
build meals to go

  

 **If a video does not have one of these labels, it's for ALL  students.**

