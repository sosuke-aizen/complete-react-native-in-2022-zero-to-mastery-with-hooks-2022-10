

In the next couple of videos, we will show you a typical Github workflow.
However, if this topic is still new to you and you find the next videos
difficult to follow, we have included an **_Appendix: Git + Github_** at the
bottom of the course that you can watch if you want more information about the
topic :)  
  
We have also included an appendix on contributing to Open Source Projects once
you learn Git and Github!

