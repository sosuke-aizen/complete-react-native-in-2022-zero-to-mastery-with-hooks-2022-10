

Congratulations! You have just learned React! It is now time for you to keep
going with the course and dive into the world of React Native. I know we are
still getting started and things may not 100% be clear for you, but we promise
that what you have learned up until now will allow you to do some amazing
things in the field of React Native. Hang in there and jump back to **The 2
Paths** section and start watching the videos in there!  
  
Ps If you want to dive deeper into this section and learn React even more in
depth, you can take the full React Web Development course by ZTM: **Complete
React Developer:  Zero to Mastery  
**

