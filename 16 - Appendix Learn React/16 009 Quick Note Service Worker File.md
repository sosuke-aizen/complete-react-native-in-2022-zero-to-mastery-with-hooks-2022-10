

In the next video, we will start building our very first React Component.
Hooray!  
  
However, remember what I said in the previous videos when I introduced you to
Create React App: CRA is constantly evolving... and the latest version of CRA
includes extra files like the Web Vitals that we will ignore for now. One file
you will see in the next video that no longer comes with the latest version of
CRA is the `registerServiceworker.js`. Therefore you can ignore the
`registerServiceworker()` in _index.js_ that you will see in the next video
since we will not be using it.  
  
  
Let get to coding!

