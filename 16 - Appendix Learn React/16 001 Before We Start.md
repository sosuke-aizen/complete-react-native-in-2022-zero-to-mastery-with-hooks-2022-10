

Heads up! In this section, we are assuming you have Node installed on your
computer and that you know how to use Git and Github. If you do not have
either of these:

1.[ **Install Node on your computer**](https://nodejs.org/en/download/) if you
do not already have it installed. We actually how to show you how to install
Node in the course as well if you need extra assistance in the: _[MAC]
Installing Node and Yarn_ and _[WINDOWS] Installing Node and Yarn_ lectures in
the **Mobile Development Professional: Local Setup** section.

2\. Go watch the section [**Appendix: Git +
Github**](https://academy.zerotomastery.io/courses/learn-react-
native/lectures/22596113) **** videos.

Alternatively, you can always use
[**https://codesandbox.io/s/new**](https://codesandbox.io/s/new) to follow
along without installing anything and just using your browser.

Otherwise, let's get started and learn React!

