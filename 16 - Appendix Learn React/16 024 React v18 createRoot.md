

A lot of the changes in React 18 don't really concern us except for this one:  

  * `createRoot`: New method to create a root to `render`. Use it instead of `ReactDOM.render`. New features in React 18 dont work without it.

  

This means that as React evolved and updates, if you want to be able to use
all of the latest features that come with it, you will have to update this
line of code in your project:  
  
Change in _index.js_ :

    
    
    ReactDOM.render(<App />, document.getElementById('root'));

  
To this:

    
    
    ReactDOM.createRoot(document.getElementById('root')).render(<App />);

  

For robofriends, this doesn't really change anything, but it's good to always
keep your codebase up to date in case you want to add onto the project in the
future. **This is an important step of a developer: keeping up to date with
changes in the libraries you use, so that you can always stay ahead and make
sure when something is needed, you don't have to spend hours figuring things
out, and why things break, because you didn't keep the project and libraries
up to date :)**

