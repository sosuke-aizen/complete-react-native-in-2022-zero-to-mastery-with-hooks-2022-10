

You can find the final repository of the finish MealsToGo App here:
[**https://github.com/mobinni/MealsToGo**](https://github.com/mobinni/MealsToGo
"https://github.com/mobinni/MealsToGo")  
  
We highly recommend you follow along with us and code your own app. Throughout
the course, we will provide for you all the code changes in the form of pull
requests and Github links so you can always make sure that after each
significant feature, you are able to double check your code and make sure
everything works. Enjoy!  
  
Ps feel free to leave a review if you are finding this course useful. We've
definitely put a lot of effort into it!

