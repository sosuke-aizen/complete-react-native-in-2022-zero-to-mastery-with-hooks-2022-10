

 **In the following videos you will see me setup MealsToGo on a MAC
environment, some of the commands I type in may not work on other Operating
Systems. DON'T PANIC!**

  

Rest assured that the main commands we run to setup project should work for
you, but because we are on a MAC environment things like  `ls` may not work on
your Windows system.

`expo init` should still work and be available to you which is the main
command we need.

If you do run into issues make sure to follow the documentation steps:
[`https://docs.expo.io/get-started/installation/`](https://docs.expo.io/get-
started/installation/)

If it's still causing issues reach out to our amazing Discord community or the
QA section!

