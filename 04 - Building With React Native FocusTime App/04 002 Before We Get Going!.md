

The flow of this section might be counter-intuitive to some of you, what do I
mean?

Well the flow of this section focuses solely on trying to get you to build an
app without the overhead of setup, etc.  
On top of that we also follow a flow that you would see in a real project,
where we build features, but we aren't building them picture perfect!

You may see warnings along the way from your linting and that's completely
fine, we address the majority of them in what I like to call **Reflection
Time**. This is where we look at what we've built and address some of the
outstanding discrepancies that linting and other things may point out.

Be aware, there is a difference between a linting warning and a linting error.
A warning is solely telling you that something might not be optimal, an error
is telling you that your code might be wrong.

If you see a warning, **DON'T  PANIC!**

