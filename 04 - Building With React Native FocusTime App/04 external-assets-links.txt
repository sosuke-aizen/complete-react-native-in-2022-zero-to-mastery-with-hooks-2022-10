
003 Expo
https://expo.io/

003 Register-Your-Account
https://expo.io/signup

004 Make-a-Snack
https://snack.expo.io

004 What-Is-A-Snack-
https://github.com/expo/snack

005 FocusTime-Demo
https://snack.expo.dev/@mobinni/focustime

008 Solution-Changes
https://github.com/mobinni/FocusTime/pull/1/files

008 Solution-Code
https://github.com/mobinni/FocusTime/tree/1.7-preparation

012 Solution-Changes
https://github.com/mobinni/FocusTime/pull/2/files

012 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.0-avoid-notch

012 SafeAreaView
https://reactnative.dev/docs/safeareaview

013 Solution-Changes
https://github.com/mobinni/FocusTime/pull/3/files

013 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.1-avoid-statusbar

013 StatusBar
https://reactnative.dev/docs/statusbar

013 Platform
https://reactnative.dev/docs/platform

014 Solution-Changes
https://github.com/mobinni/FocusTime/pull/4/files

014 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.2-styling-background

015 Solution-Changes
https://github.com/mobinni/FocusTime/pull/5

015 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.3-setup-focus-feature

016 Solution-Changes
https://github.com/mobinni/FocusTime/pull/6/files

016 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.4-text-input

016 TextInput
https://callstack.github.io/react-native-paper/text-input.html

017 Solution-Changes
https://github.com/mobinni/FocusTime/pull/7

017 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.5-storing-subject

017 useState
https://reactjs.org/docs/hooks-state.html

018 Rounded-Button
https://github.com/mobinni/FocusTime/pull/8/files

018 Solution-Changes
https://github.com/mobinni/FocusTime/pull/9/files

018 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.6-adding-a-button

019 Solution-Changes
https://github.com/mobinni/FocusTime/pull/10/files

019 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.7

020 Solution-Changes
https://github.com/mobinni/FocusTime/pull/11/files

020 Solution-Code
https://github.com/mobinni/FocusTime/tree/2.8

021 Solution-Changes
https://github.com/mobinni/FocusTime/pull/12/files

021 Solution-Code
https://github.com/mobinni/FocusTime/tree/countdown

022 Solution-Changes
https://github.com/mobinni/FocusTime/pull/13/files

022 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.0

023 Solution-Changes
https://github.com/mobinni/FocusTime/pull/14/files

023 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.1

024 Solution-Changes
https://github.com/mobinni/FocusTime/pull/15/files

024 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.2

025 Solution-Changes
https://github.com/mobinni/FocusTime/pull/16/files

025 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.3

026 Solution-Changes
https://github.com/mobinni/FocusTime/pull/17/files

026 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.4

027 Solution-Changes
https://github.com/mobinni/FocusTime/pull/18/files

027 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.5

028 Solution-Changes
https://github.com/mobinni/FocusTime/pull/19/files

028 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.6

029 Solution-Changes
https://github.com/mobinni/FocusTime/pull/20/files

029 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.7

030 Solution-Changes
https://github.com/mobinni/FocusTime/pull/21/files

030 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.8

031 Solution-Changes
https://github.com/mobinni/FocusTime/pull/22/files

031 Solution-Code
https://github.com/mobinni/FocusTime/tree/3.9

032 Solution-Changes
https://github.com/mobinni/FocusTime/pull/23/files

032 Solution-Code
https://github.com/mobinni/FocusTime/tree/4.0
