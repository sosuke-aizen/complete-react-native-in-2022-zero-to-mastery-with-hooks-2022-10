#### **1\. Before we get started, let's do a quick exercise that will take
less than 3 minutes:**

  

 **Step 1** \- Click on this link to join our Private Online Classroom:
[**CLICK HERE TO JOIN NOW**](https://discord.gg/9KxUUxt7Vd) **.**

  
 **Step 2 -   **Once inside, go to **#introductions** channel and share who
you are, where you are from, and why you chose to do this course.  
  
**Step 3 -  **Go to the **#accountability-buddies** and find a buddy who is
starting a ZTM course today just like you _(doesn't have to be the same
course)_! You will be keeping each other accountable throughout the course and
motivating each other to finish. Who knows, maybe you will find life long
friends/coworkers this way.  

 **Step 4 -** check out **#general** or your course specific channel to meet
others in your class. We will be announcing class hangouts and instructor
meetups/livestreams in there. Some of the channels available for you to join
are:

  
 ** _#react-native -  _** _For all questions  Mobile Development and React
Native  
_ ** _#alumni -  _** _Ask graduates of this course questions  
_ ** _#womenintech -_** _For the female coders out there  
_ ** _#job-hunting -_** _Anything related to finding a job as a developer  
_

 _....and many many more!!_

  

 **OPTIONAL Step -** A fellow ZTM student, Matt, has made a video walkthrough
for you of the community [**here**](https://youtu.be/TVI_MBoCOak) **** if you
want to watch and take a deep dive as to all the things that happen in the
community every month.  

  

####  **2\. Course updates and cheatsheets?  
**

We are constantly adding new videos and updating this course, announcing new
community events, and creating course cheatsheets/resources. To stay up to
date with all the latest changes to the course and new videos, keep an eye out
on the **#announcements** channel in our Discord community. Anything important
will be announced in there _(you can scroll to see past announcements)_.

**  
You can also follow me on twitter where I keep you updated about industry
news, upcoming courses, and random thoughts on life:**[
**@andreineagoie**](https://twitter.com/AndreiNeagoie) **and**[
**@zerotomasteryio**](https://twitter.com/zerotomasteryio)  

####  
  
 **3\. One more thing...**

  

As you start the course, Udemy will have a popup that  will ask you to leave a
review of the course. Yes, it can get a little annoying _(it's a Udemy system
and not something we have control over)_ but please leave an honest review
_(even if you just started the course)_ as it really helps us out and allows
more people to discover this course in this massively competitive marketplace.
It would truly mean a lot . **  
**

 **  
  
  
**Thank you and welcome to ZTM,  
  
\- Andrei

  

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2021-03-09_13-54-24-8b2ccf9752b026233a7ebd195d792254.png)

  

