

In the previous video we saw an approach to taking a picture. Depending on the
Android device you own it will render out the camera module differently. Some
additional fixes may be required to allow android to properly snap a photo.

In this scenario we looked at changing our implementation so it can work both
on iOS and Android appropriately.

  

Also our usage of  `useFocusEffect` was not utilized according to the
documentation so additional changes are required there.

  

Be aware with these changes on Android you may still be required to give a
`ratio` prop to the camera for instance

`<ProfileCamera ref={(camera) => (cameraRef.current = camera)}
type={Camera.Constants.Type.front} ratio={"16:9"} >`

  

You can read up on this in the docs: [Camera - Expo
Documentation](https://docs.expo.io/versions/latest/sdk/camera/)

[Solution Changes](https://github.com/mobinni/MealsToGo/pull/146/files)

[Solution Repo](https://github.com/mobinni/MealsToGo/tree/bonus-camera-5)

