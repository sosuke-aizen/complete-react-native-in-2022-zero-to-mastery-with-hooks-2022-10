

If you haven't done so already, **it is time to contribute to your first open
source project!**

  

The contribution can be as big as a new feature, or as small as fixing a typo
in the `**README.md**` file. Here is what we are going to do:

  

1\. Join our Discord community **(see  Lecture 2 of this course) **if you
haven't yet.

2\. [**Read this step by step guide by selecting "Open Source
Projects"**](https://zerotomastery.io/community/?utm_source=udemy&utm_medium=coursecontent)
**** on the page.

3. **In order to see the Zero to Mastery Icon in your Github profile** , follow this: <https://help.github.com/articles/publicizing-or-hiding-organization-membership/>. If you have any issues with this, you can tag the management team on Discord and let them know by pinging **@Management Team**

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2020-10-29_14-13-59-195158c6b12518aff618e4609f18e9c2.png)

  

  

