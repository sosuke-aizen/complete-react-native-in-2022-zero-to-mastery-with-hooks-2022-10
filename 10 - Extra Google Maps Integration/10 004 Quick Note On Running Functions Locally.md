

In the next video I will be showing you how run your firebase functions
locally, **make sure** that you run the command to start the functions inside
of the folder of the function itself, do not try to start your firebase
function from any other folder or it will **NOT  **work.

As an example, if your folder is

` MealsToGo/`

  

Make sure in your terminal you navigate to your functions folder, by running
the command:

`cd functions`

