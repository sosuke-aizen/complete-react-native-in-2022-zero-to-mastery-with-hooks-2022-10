

I wanted to leave this quick note here to address a minor semantic issue. I
misspoke in the next video and referenced `contentContainerStyle` as the
styling that is applied to each individual item.

However it is not applied to each item individually rather it is applied to
the holistic content inside the container. That is why you need to wrap the
`Spacer` component around the rendered item.

For more info read up on content container style: [ScrollView React
Native](https://reactnative.dev/docs/scrollview#contentcontainerstyle)

