

Before we get going I thought it would be of importance to point something
out.

Unlike with FocusTime, when we start building locally we can **only** run our
app on our phone **when we are running expo locally**.

  
What this means is that if you shut your laptop, or turn off expo you won't be
able to see your app unless you start expo again. It's important to know that
this is **NOT** a limitation but how expo development works. This is what we
call a **local build.**

 _You have a "local" build that will run while your development environment is
running._

In the flow of the course we assume that you build all of meals to go's main
flow before "publishing" your app to expo which will allow you to access the
version of your app that you publish without having the run the local
environment. If you're interested in doing this throughout the course rather
then at the end check out our video on **Expo Publish** or [read the
docs](https://docs.expo.io/workflow/publishing/)

  
 _Note here that "publishing your app to expo" is_ ** _NOT_** _the same as
publishing your app to the app store, we will cover that in a bonus section._

  

