

In the following video you will be building out a spacer component, now I will
quickly want to **remind** you that the videos in this course follow-up on
each-other which means we might build something, test it in iOS and it breaks
in Android.

This is completely normal and follows the flow of the course **DON'T  PANIC,
DON'T START A QA THREAD. **Follow along and all will point itself out.

If something starts breaking on Android, look ahead in the video list because
there's a 99% chance we're covering this **at most** the next video or 2
further along.

Again, breathe, have a sip of your favourite beverage of choice and **enjoy
the course!**

(P.S.: If wherever your from has a cool unknown beverage that we should try if
we visit your country let us know in the Discord and make sure to tag me!!)

  

